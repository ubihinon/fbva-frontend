FROM nginx:alpine

COPY nginx.conf /etc/nginx/.conf

WORKDIR /usr/share/nginx/html
COPY dist/ .
#EXPOSE 3000
# server_name vocabulary-plus.herokuapp.com;