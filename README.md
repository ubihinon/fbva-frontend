This is a front-end for a web application, which helps to improve vocabulary using interval method.
Currently it has next features:
1. load translations from the internet for a word in a card
2. load images for a word in a card
3. load pronunciations for a word in a card
4. learn words using interval method
