import {
  Component, OnInit, AfterViewInit,
  ViewChildren
} from '@angular/core';
import { Tag } from '../shared/model/Tag';
import { TagService } from '../shared/services/tag.service';
// import { AppContextMenuDirective } from '../context-menu.directive';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/';
import { CardService } from '../shared/services/card.service';
import { async } from 'rxjs/scheduler/async';
import { Card } from '../shared/model/Card';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent implements OnInit, AfterViewInit {

  public tags: Observable<Array<Tag>> | null = null;
  // private cards: Observable<Array<Card>> | null = null;
  private editingTag: Tag;
  private selectedTag: Tag;
  // private cards: Cards[] = [];
  private cards: Observable<Array<Card>> | null = null;
  protected tagCountToLearn: Map<string, number> = new Map();

  @ViewChildren('inputEditTag') inputEditTags;

  constructor(private tagService: TagService,
              protected cardService: CardService,
              private router: Router) {
  }

  ngOnInit() {
    this.selectedTag = new Tag();
    this.resetEditingTag();

    this.tags = this.tagService.tagsBehaviour;

    this.cards = this.cardService.cardsBehaviour;
    this.cards.subscribe(cards => {
      if (cards != null) {
        this.tags.subscribe(tags => {
          if(tags != null) {
            for (let tag of tags) {
              this.tagCountToLearn.set(tag.name, this.cardService.getCardsByTag(tag).length);
            }
          }
        });
      }
    });
    // this.cards.subscribe(=>);
    // this.cards = this.cardService.cardsBehaviour;
    // this.cardService.cardsBehaviour.subscribe((card) => {
    // this.cards.push(card);
    // console.log(`TagListComponent SUBSCRIBE: ${JSON.stringify(card)}`);
    // });
    // this.getCountCardsByTag();
  }

  protected learn(tag: Tag): void {
    this.cardService.cardsToLearn = this.cardService.getCardsByTag(tag);
    this.router.navigate(['/learning']).then();
  }

  protected getCountCardsByTag(): void {
    this.cardService.cardsBehaviour.subscribe((card) => {
      // this.cards.push(card);
      console.log(`TagListComponent SUBSCRIBE: ${JSON.stringify(card)}`);
      console.log('TAGGGG SERVICE: ' + this.tags);
      // for (let tag of this.tagService.tags) {
      //   console.log('TAGGGG: ' + tag.name);
      //   this.tagCountToLearn.set(tag.name, cardService.getCardsByTag(tag));
      // }
    });
  }

  ngAfterViewInit(): void {
  }

  protected resetEditingTag(): void {
    this.editingTag = new Tag();
  }

  protected setEditingTag(tag: Tag): void {
    this.editingTag = tag;
    setTimeout(this.setFocusInputEditTag, 50, this.editingTag, this.inputEditTags);
  }

  private setFocusInputEditTag(tag: Tag, inputEditTags): void {
    inputEditTags.forEach(elem => {
      if (elem.nativeElement.value == tag.name) {
        elem.nativeElement.focus();
        return;
      }
    });
  }

  protected save(value: string): void {
    let newTagName = value.trim();
    if (Tag.tagIsRight(newTagName)) {
      if (newTagName != '' && this.editingTag.name != newTagName) {
        this.editingTag.name = newTagName;
        this.tagService.update(this.editingTag).subscribe();
      }
    }
    this.resetEditingTag();
  }

  protected deleteTag(tag: Tag): void {
    this.tagService.remove(tag).subscribe();
  }

  protected tagSelected(tag: Tag): void {
    this.selectedTag = tag;
    this.tagService.selectedTagEvent.emit(tag);
    this.router.navigate(['/card-list']).then();
  }
}
