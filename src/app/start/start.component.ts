import {Component, OnInit} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {Title} from '@angular/platform-browser';
import * as $ from 'jquery';

@Component({
    moduleId: 'module.id',
    selector: 'start',
    templateUrl: 'start.component.html',
    styleUrls: ['start.component.scss']
})
export class StartComponent implements OnInit {

    public message: string = '';

    constructor(private titleService: Title, protected authService: AuthService) {
        this.titleService.setTitle('Start');
    }

    ngOnInit(): void {
    }

    public closeSignUpWindow(): void {
        $('#registryModal').modal('close');
    }

    public closeLoginWindow(): void {
        $('#loginModal').modal('close');
    }

    public setMessage(value: string) {
        this.message = value;
    }
}
