import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';

import { Observable } from 'rxjs/';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise'
import { NGXLogger } from 'ngx-logger';

@Component({
  moduleId: 'module.id',
  selector: 'auth',
  templateUrl: 'auth.component.html',
  styleUrls: ['auth.component.scss']
})
export class AuthComponent implements OnInit {
  username: string;
  password: string;
  loading: boolean = false;
  error: string = '';
  @Output() closeButtonLoginClickedEvent = new EventEmitter<boolean>();

  constructor(private authService: AuthService,
              private router: Router,
              private logger: NGXLogger) {
  }

  ngOnInit(): void {
    this.authService.logout();
  }

  public login(): void {
    if (this.username == null
      || this.username == ''
      || this.password == null
      || this.password == '') {
      return;
    }
    this.loading = true;
    this.authService.login(this.username, this.password)
      .toPromise()
      .then(() => {
        this.logger.debug(`User ${this.username} logged`);
        this.close();
        this.router.navigate(['/']).then();
      }).catch(() => {
      this.error = 'Username or password is incorrect';
      this.logger.debug(`${this.error}`);
      this.loading = false;
    });
  }

  public close(): void {
    this.closeButtonLoginClickedEvent.emit(true);
  }
}
