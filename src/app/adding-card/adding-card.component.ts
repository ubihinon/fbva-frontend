import {
  Component, OnInit, AfterViewInit, ViewChild
} from '@angular/core';
import { YandexSearchService } from '../shared/services/yandexsearch.service';
import { CardService } from '../shared/services/card.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/debounceTime';
import { Tag } from '../shared/model/Tag';
import { Translation } from '../shared/model/Translation';
import { Word } from '../shared/model/Word';
import { Language } from '../shared/model/Language';
import { NGXLogger } from 'ngx-logger';
import { SettingsService } from '../shared/services/settings.service';
import { LanguageService } from '../shared/services/language.service';
import { SettingName } from '../shared/constants/SettingName';

@Component({
  selector: 'adding-card',
  templateUrl: './adding-card.component.html',
  styleUrls: ['./adding-card.component.scss'],
  providers: [NGXLogger]
})
export class AddingCardComponent implements OnInit, AfterViewInit {
  public wordName: string = '';
  public translations: Translation[];
  public selectedTranslations: Translation[];
  public customTranslations: Translation[];
  public showCustomTranslations: boolean;
  public selectedTags: Tag[] = [];
  public languages: Language[] = [];
  public sourceLanguage: Language;
  public targetLanguage: Language;
  @ViewChild('inputWordName') inputWordName;

  constructor(private logger: NGXLogger,
              private yandexSearchService: YandexSearchService,
              private cardService: CardService,
              private languageService: LanguageService,
              private settingsService: SettingsService) {

  }

  ngOnInit() {
    this.selectedTranslations = [];
    this.translations = [];
    this.customTranslations = [];
    this.languages = [];
    this.showCustomTranslations = false;
    this.languageService.getAll().subscribe(languages => {
      this.languages = languages;
      this.init_languages();
    });
  }

  ngAfterViewInit(): void {
    this.inputWordName.nativeElement.focus();
  }

  private init_languages(): void {
    if (this.languages.length == 0) {
      return;
    }
    this.sourceLanguage = this.languages[0];
    this.targetLanguage = this.languages[1];
    for (let language of this.languages) {
      if (
        language.code
        ==
        this.settingsService.getValue(SettingName.SOURCE_LANGUAGE.toString())
      ) {
        this.sourceLanguage = language;
      }
      if (
        language.code
        ==
        this.settingsService.getValue(SettingName.TARGET_LANGUAGE.toString())
      ) {
        this.targetLanguage = language;
      }
    }
  }

  public searchWord(): void {
    this.logger.debug(`Search word ${this.wordName}`);
    this.showCustomTranslations = true;
    this.translations = [];
    if (this.wordName != '') {
      this.showCustomTranslations = true;
      this.yandexSearchService.getTranslationsByWordName(
        this.wordName,
        this.sourceLanguage,
        this.targetLanguage
      ).subscribe(translations => {
          this.translations = translations;
        });
    }
  }

  public addCard(): void {
    if (
      this.wordName != ''
      && (this.selectedTranslations.length > 0 || this.customTranslations.length > 0)
    ) {
      this.selectedTranslations = this.selectedTranslations.concat(this.customTranslations);

      this.cardService.create(
        this.assembleWord(),
        this.selectedTags,
        this.sourceLanguage,
        this.targetLanguage
      ).subscribe();
      this.wordName = '';
      this.translations = [];
      this.selectedTranslations = [];
      this.customTranslations = [];
      this.showCustomTranslations = false;
      this.inputWordName.nativeElement.focus();
    }
  }

  private assembleWord(): Word {
    return new Word(this.wordName, this.selectedTranslations)
  }

  public addSelectedTranslation(translations: Translation[]): void {
    this.selectedTranslations = translations;
  }

  protected addCustomTranslations(translations: Translation[]) {
    this.customTranslations = translations;
  }

  public addTags(tags: Tag[]): void {
    this.selectedTags = tags;
  }

  public changeSourceLanguage(): void {
    this.logger.debug(`Source language changed to ${this.sourceLanguage.name}`);
    this.settingsService.save(SettingName.SOURCE_LANGUAGE.toString(), this.sourceLanguage.code);
  }

  public changeTargetLanguage(): void {
    this.logger.debug(`Target language changed to ${this.targetLanguage.name}`);
    this.settingsService.save(SettingName.TARGET_LANGUAGE.toString(), this.targetLanguage.code);
  }
}
