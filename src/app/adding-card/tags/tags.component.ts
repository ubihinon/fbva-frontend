import { Component, OnInit, AfterViewInit, Output, EventEmitter, Input } from '@angular/core';
import { Tag } from '../../shared/model/Tag';
import { TagService } from '../../shared/services/tag.service';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
})
export class TagsComponent implements OnInit, AfterViewInit {

  public tags: Tag[];
  public tagsBehaviour: Observable<Array<Tag>> | null = null;
  @Input() public selectedTags: Tag[] = [];
  @Output() addedTagEvent: EventEmitter<Tag[]> = new EventEmitter();

  constructor(private tagService: TagService,
              private logger: NGXLogger) {
  }

  ngOnInit() {
    this.tagsBehaviour = this.tagService.tagsBehaviour;
    this.tagsBehaviour.subscribe(tags => {
      this.tags = tags;
      this.updateSelectedTags();
    })
  }

  ngAfterViewInit(): void {
  }

  private updateSelectedTags(): void {
    for (let selectedTag of this.selectedTags) {
      let tagToChange = this.findTagById(selectedTag);
      if (tagToChange != null) {
        this.replaceTag(selectedTag, tagToChange);
      } else {
        this.deleteTag(selectedTag);
      }
    }
  }

  private findTagById(tagToFind: Tag): Tag {
    for (let tag of this.tags) {
      if (tagToFind.id == tag.id) {
        return tag;
      }
    }
    return null;
  }

  private replaceTag(tag: Tag, tagToChange: Tag): void {
    this.selectedTags.splice(this.selectedTags.indexOf(tag), 1, tagToChange)
  }

  private deleteTag(tag: Tag): void {
    this.selectedTags.splice(this.selectedTags.indexOf(tag), 1)
  }

  public onAdd(tag): void {
    let tagName = (tag instanceof Tag) ? tag.name : tag;
    if (this.findTagByName(tagName, this.tags) == null) {

      this.tagService.add(new Tag(null, tagName)).subscribe(addedTag => {
        this.logger.debug(`Add tag ${addedTag.name}`);
        this.tags.push(addedTag);
        this.replaceTagByName(addedTag, this.selectedTags);
      });
    }
    this.addedTagEvent.emit(this.selectedTags);
  }

  public onRemove() {
    this.addedTagEvent.emit(this.selectedTags);
  }

  private findTagByName(tagName, tags: Tag[]): Tag {
    for (let tag of tags) {
      if (tag.name == tagName) {
        return tag;
      }
    }
    return null;
  }

  private replaceTagByName(tag, tags: Tag[]): boolean {
    for (let tagIndex in tags) {
      if (tags[tagIndex].name == tag.name) {
        tags[tagIndex].id = tag.id;
        tags[tagIndex].name = tag.name;
        return true;
      }
    }
    return false;
  }

  public onAdding(tag): Observable<any> {
    let canAdd = true;
    if (!(tag instanceof Object)) {
      if (!Tag.tagIsRight(tag)) {
        canAdd = false;
      }
    }
    return of(tag).pipe(
         filter(addingTag => canAdd)
    );
  }
}
