import {
  Component, OnInit, Output, EventEmitter
} from '@angular/core';
import { LanguageService } from '../shared/services/language.service';
import { Language } from '../shared/model/Language';
import { UserService } from '../shared/services/user.service';
import { SettingsComponent } from '../settings/settings.component';
import { User } from '../shared/model/User';

@Component({
  moduleId: 'module.id',
  selector: 'registry',
  templateUrl: 'registry.component.html',
  styleUrls: ['registry.component.scss'],
  providers: [SettingsComponent]
})
export class RegistryComponent implements OnInit {
  languages: Language[] = [];
  username: string = '';
  password: string = '';
  email: string = '';
  userError: boolean = false;
  emailError: boolean = false;
  passwordError: boolean = false;
  selectedLanguage: Language = null;
  mapErrors: Map<string, string>;
  @Output() closeButtonSignUpClickedEvent = new EventEmitter<boolean>();
  @Output() signedUpSuccessfullyEvent = new EventEmitter<string>();

  constructor(private languageService: LanguageService,
              private userService: UserService) {
    this.mapErrors = new Map<string, string>();
  }

  ngOnInit(): void {
    this.languageService.getAll().subscribe(languages => {
      this.languages = languages;
      this.selectedLanguage = this.languages[0];
    });
  }

  public signUp(): void {
    this.userService.save(this.username, this.password, this.email, this.selectedLanguage)
      .subscribe(() => {
        this.signedUpSuccessfullyEvent.emit('You\'ve signed up successfully. ' +
          'Now You can sign in using your username and password');
        this.closeButtonSignUpClickedEvent.emit(true);
      })
  }

  public checkUsername(): void {
    this.mapErrors.delete('userError');
    this.mapErrors.delete('usernameRegexWrong');
    if (User.usernameIsRight(this.username)) {
      this.userService.getByParameter(this.username, 'username')
        .subscribe(user => {
          this.userError = user != null;
          if (this.userError && this.username.length > 0) {
            this.mapErrors.set('userError', 'This user already exists');
          }
        });
    } else {
      this.userError = true;
      this.mapErrors.set(
        'usernameRegexWrong',
        'Username is required. Minimum 3 and maximum 30 characters. ' +
        'Letters, digits and @/./+/-/_ only.'
      );
    }
  }

  public checkEmail(): void {
    this.mapErrors.delete('emailError');

    if (User.emailIsRight(this.email)) {
      this.mapErrors.delete('emailRegexWrong');

      this.userService.getByParameter(this.email, 'email')
        .then(user => {
          this.emailError = user != null;
          if (this.emailError && this.email.length > 0) {
            this.mapErrors.set('emailError', 'Email is already found in our database');
          }
        });
    } else {
      this.emailError = true;
      this.mapErrors.set('emailRegexWrong', 'You must type correct email');
    }
  }

  public checkPassword(): void {
    if (!User.passwordIsRight(this.password) && this.password.length > 0) {
      this.mapErrors.set(
        'passwordWrong',
        'Password must have minimum 8 and maximum 128 characters ' +
        'and must contain of numbers and letters'
      );
      this.passwordError = true;
    } else {
      this.mapErrors.delete('passwordWrong');
      this.passwordError = false;
    }
  }

  public close(): void {
    this.closeButtonSignUpClickedEvent.emit(false);
  }
}
