import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { Card } from '../model/Card';

@Pipe({
  name: 'searchfilter'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
  transform(cards: Card[], value: string, length: boolean): any[] {
    value = value.trim();
    if (value == '') {
      return cards;
    }
    if (!cards) {
      return [];
    }
    let filteredCards = [];
    for (let card of cards) {
      if (card.word.word.indexOf(value) != -1) {
        filteredCards.push(card);
      }
      for (let translation of card.word.translations) {
        if (translation.text.indexOf(value) != -1) {
          if (filteredCards.indexOf(card) == -1) {
            filteredCards.push(card);
          }
        }
      }
    }
    return filteredCards;
  }
}
