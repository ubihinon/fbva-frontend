import { Card } from './Card';

export class LearningSession {
  public wroteToDB: boolean = false;
  public card: Card = null;
  private countRightAnswers: number = 0;

  constructor(card: Card) {
    this.card = card;
  }

  public increaseCountRightAnswers(): void {
    this.countRightAnswers++;
  }

  public resetCountRightAnswers(): void {
    this.countRightAnswers = 0;
  }

  public getCountRightAnswers(): number {
    return this.countRightAnswers;
  }
}
