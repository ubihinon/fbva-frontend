export class Translation {
  text: string = '';
  pos: string = '';

  constructor(text: string, pos: string) {
    this.text = text;
    this.pos = pos;
  }
}
