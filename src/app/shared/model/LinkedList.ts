import { es5ClassFix } from '../decorators/es5ClassFix';

@es5ClassFix()
export class LinkedList<T> extends Array<T> {
  private index: number = 0;

  constructor(arrayLength: number = 1) {
    super(arrayLength);
  }

  public next(): T {
    this.index += 1;
    if (this.index >= this.length) {
      this.index = 0;
    }
    return this[this.index]
  }

  public getValue(): T {
    return this[this.index];
  }

  public clear(): void {
    this.splice(0, this.length);
    this.index = 0;
  }
}
