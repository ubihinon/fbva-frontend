export class IntervalCard {
  lastShown: string;
  nextShow: string;

  constructor(lastShown?: string, nextShow?: string) {
    this.lastShown = lastShown;
    this.nextShow = nextShow;
  }
}
