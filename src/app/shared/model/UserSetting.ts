export class UserSetting {
    public setting: number;
    public value: string;

    constructor(name?: number, value?: string) {
        this.setting = name;
        this.value = value;
    }
}