import { Translation } from './Translation';

export class Word {
  word: string;
  translations: Translation[];

  constructor(word: string, translations: Translation[]) {
    this.word = word;
    this.translations = translations;
  }
}
