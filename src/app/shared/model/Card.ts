import { Word } from './Word';
import { Tag } from './Tag';
import { Language } from './Language';
import { IntervalCard } from './IntervalCard';

export class Card {
  id: number;
  rating: number;
  iKnowIt: string;
  word: Word;
  forkCard: number;
  cardCreator: number;
  tags: Tag[];
  sourceLanguage: Language;
  targetLanguage: Language;
  pronunciationPath: string;
  intervalCard: IntervalCard;

  constructor(id?: number,
              rating?: number,
              iKnowIt?: string,
              word?: Word,
              forkCard?: number,
              cardCreator?: number,
              tags?: Tag[],
              sourceLanguage?: Language,
              targetLanguage?: Language,
              pronunciationPath?: string,
              intervalCard?: IntervalCard) {
    this.id = id;
    this.rating = rating;
    this.iKnowIt = iKnowIt;
    this.word = word;
    this.forkCard = forkCard;
    this.cardCreator = cardCreator;
    this.tags = tags;
    this.pronunciationPath = pronunciationPath;
    this.sourceLanguage = sourceLanguage;
    this.targetLanguage = targetLanguage;
    this.intervalCard = intervalCard;
  }

  public static clone(card: Card) {
    return JSON.parse(JSON.stringify(card));
  }
}
