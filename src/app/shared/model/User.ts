import { Language } from './Language';

export class User {
  id: number;
  username: string;
  password: string;
  email: string;
  motherTongue: Language;
  birthday: Date;
  sex: string;
  profilePicture: Blob;
  active: boolean;
  staff: boolean;
  superuser: boolean;
  lastLogin: Date;
  dataJoined: Date;

  constructor(id: number,
              username: string,
              password: string,
              email: string,
              motherTongue: Language,
              birthday: Date,
              sex: string,
              profilePicture: Blob,
              active: boolean,
              staff: boolean,
              superuser: boolean,
              lastLogin: Date,
              dataJoined: Date) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.email = email;
    this.motherTongue = motherTongue;
    this.birthday = birthday;
    this.sex = sex;
    this.profilePicture = profilePicture;
    this.active = active;
    this.staff = staff;
    this.superuser = superuser;
    this.lastLogin = lastLogin;
    this.dataJoined = dataJoined;
  }

  public static usernameIsRight(username: string): boolean {
    let regex = '^[a-zA-Z0-9@_\\.\\+\\-]{3,30}$';
    return username.match(regex) != null;
  }

  public static emailIsRight(email: string): boolean {
    let regex = '^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\.([a-zA-Z]{2,5})$';
    return email.match(regex) != null;
  }

  public static passwordIsRight(password: string): boolean {
    let regex = '^(((?=.*?[0-9])((?=.*?[a-zA-Z])|(?=.*?[#?!@$%^&*-])))' +
      '|((?=.*?[a-zA-Z])(((?=.*?[0-9]))|(?=.*?[#?!@$%^&*-])))' +
      '|((?=.*?[#?!@$%^&*-])(((?=.*?[0-9]))|(?=.*?[a-zA-Z])))).{8,128}$';
    return password.match(regex) != null;
  }
}
