export class Setting {
  public name: string;
  public value: string;
  public defaultValue: string;

  constructor(name?: string, value?: string, defaultValue?: string) {
    this.name = name;
    this.value = value;
    this.defaultValue = defaultValue;
  }
}
