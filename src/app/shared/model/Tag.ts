export class Tag {
  id: number;
  name: string;

  constructor(id?: number, name?: string) {
    this.id = id;
    this.name = name;
  }

  public static tagIsRight(tag: string): boolean {
    let regex = new RegExp('^[0-9a-zA-Z_]{0,20}$');
    return regex.test(tag) != null;
  }
}
