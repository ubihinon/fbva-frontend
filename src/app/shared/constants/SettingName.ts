export enum SettingName {
  SOURCE_LANGUAGE = 'source_language',
  TARGET_LANGUAGE = 'target_language',
  AUTO_PRONUNCIATION = 'auto_pronunciation'
}
