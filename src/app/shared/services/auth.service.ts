import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/';
import 'rxjs/add/operator/map';
import { User } from '../model/User';
import { UserService } from './user.service';
import { Language } from '../model/Language';
import { BaseService } from './base.service';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';
import { environment } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class AuthService extends BaseService {
  public token: string = null;
  currentUser: User;

  constructor(private http: HttpClient,
              private userService: UserService,
              public logger: NGXLogger,
              private router: Router) {
    super();
    this.currentUser = new User(null, null, null, null, new Language(null, null, null),
      null, null, null, null, null, null, null, null);

    if (localStorage.getItem('currentUserToken') != null
      && localStorage.getItem('currentUser') != null) {
      this.token = JSON.parse(localStorage.getItem('currentUserToken')).token;
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
  }

  public login(username: string, password: string): Observable<boolean> {
    this.userService.getByParameter(username, 'username')
      .subscribe(user => {
        this.currentUser = user;
        localStorage.setItem('currentUser', JSON.stringify(user));
      });
    // .catch(error => this.logger.error(`While login user the error occurred: ${error}`));

    return this.http.post(environment.API_URL + '/api/users/token-auth/',
      JSON.stringify({username: username, password: password}),
      this.getJSONContentTypeHeaderNew())
      .map((response: any) => {
        let token = response.token;
        if (token) {
          this.token = token;
          localStorage.setItem('currentUserToken',
            JSON.stringify({username: username, token: token}));
          this.logger.debug(`User ${username} authorized successfully`);
          return true;
        } else {
          this.logger.debug(`Failed to authorize the user ${username}`);
          return false;
        }
      });
  }

  public refreshToken(): void {
    this.http.post(
      environment.API_URL + '/api/users/token-refresh/',
      JSON.stringify({token: JSON.parse(localStorage.getItem('currentUserToken')).token},),
      this.getAuthorizationHeaderNew()
    ).map((response: any) => {
      console.log(response)
    }).subscribe();
  }

  public logout(): void {
    if (this.currentUser) {
      this.logger.debug(`User ${this.currentUser.username} logout`);
    }
    this.token = null;
    this.currentUser = null;
    localStorage.removeItem('currentUserToken');
    localStorage.removeItem('currentUser');
    this.router.navigate(['/start']).then();
  }

  public saveUser(): void {
    localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
  }

  public getUser(): User {
    return JSON.parse(localStorage.getItem('currentUser'))
  }
}
