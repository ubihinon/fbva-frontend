import { Injectable } from '@angular/core';
import { Translation } from '../model/Translation';
import { BaseService } from './base.service';
import { Language } from '../model/Language';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { catchError } from "rxjs/operators";

@Injectable()
export class YandexSearchService extends BaseService {
  words: Translation[] = [];

  constructor(private http: HttpClient, public logger: NGXLogger) {
    super();
  }

  public getTranslationsByWordName(wordName: string,
                                   sourceLanguage: Language,
                                   targetLanguage: Language): Observable<Translation[]> {
    this.words = [];
    return this.http.get('https://dictionary.yandex.net/api/v1/dicservice.json/lookup?' +
      'key=dict.1.1.20170718T021319Z.fd5b241226326587.f84033250f87d92572243942d4910c3bac880f55' +
      '&lang=' + sourceLanguage.code + '-' + targetLanguage.code +
      '&text=' + wordName)
      .map((json: any) => {
        if (json.def[0] != undefined) {
          for (let currentWord of json.def[0].tr) {
            let word = new Translation(currentWord.text, currentWord.pos);
            this.words.push(word);
          }
        }
        return this.words;
      })
      .pipe(catchError(this.handleError))
  }
}
