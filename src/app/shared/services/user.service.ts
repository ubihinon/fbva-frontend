import { Injectable } from '@angular/core';
import { User } from '../model/User';

import { Observable } from 'rxjs';
import { Language } from '../model/Language';
import { BaseService } from './base.service';
import { NGXLogger } from 'ngx-logger';
import { environment } from "../../../environments/environment";
import { catchError } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class UserService extends BaseService {
  users: User[] = [];

  constructor(private http: HttpClient,
              public logger: NGXLogger) {
    super()
  }

  public save(username: string,
              password: string,
              email: string,
              language: Language): Observable<Object> {
    let user = new User(null,
      username,
      password,
      email,
      language,
      null, null, null, true, true, true, null, null);
    this.logger.debug(`User ${user.username} saved`);
    return this.http.post(environment.API_URL + '/api/users/', user)
      .pipe(catchError(this.handleError));
  }

  public update(user: User): Observable<any> {
    return this.http.put(environment.API_URL + '/api/users/' + user.id + '/', user,
      this.getAuthorizationHeaderNew())
      .map(data => data)
      .pipe(catchError(this.handleError));
  }

  public getByParameter(search_str: string, searchBy: string): Observable<User> {
    return this.http.get(environment.API_URL + '/api/users/?search='
      + search_str + '&exact=true&search-by=' + searchBy)
      .map((user: User) => {
        if (user[0] != null) {
          return user[0];
        }
      })
      .pipe(catchError(this.handleError));
  }

  public getListByParameter(search_str: string, searchBy: string): Observable<Object> {
    return this.http.get(environment.API_URL + '/api/users/?search='
      + search_str + '&exact=true&search-by=' + searchBy)
      .map((users) => {
        if (users != null) {
          return users;
        }
      })
      .pipe(catchError(this.handleError));
  }

  public changePassword(currentPassword: string, newPassword: string, user: User): Observable<any> {
    return this.http.put(
      environment.API_URL + '/api/users/' + user.id + '/update-password/',
      JSON.stringify({oldPassword: currentPassword, newPassword: newPassword}),
      this.getAuthorizationHeaderNew()
    )
      .map(data => data)
      .pipe(catchError(this.handleError));
  }
}
