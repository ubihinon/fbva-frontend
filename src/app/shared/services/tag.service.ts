import { Tag } from '../model/Tag';
import { Injectable, EventEmitter, Inject } from '@angular/core';
import { AuthService } from './auth.service';

import { Observable, BehaviorSubject } from 'rxjs';
import { BaseService } from './base.service';
import { NGXLogger } from 'ngx-logger';
import {environment} from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { catchError } from "rxjs/operators";

@Injectable()
export class TagService extends BaseService {
  private _tagsBehavior: BehaviorSubject<Tag[]>;
  public tags: Tag[];
  public selectedTagEvent: EventEmitter<Tag> = new EventEmitter();

  constructor(private http: HttpClient,
              public logger: NGXLogger,
              @Inject(AuthService) authService: AuthService) {
    super(authService);
    this._tagsBehavior = new BehaviorSubject(this.tags);
  }

  public loadTagsForCurrentUser(): Observable<Tag[]> {
    return this.http.get(environment.API_URL + '/api/tags/?user_id=' + this.authService.getUser().id,
      this.getAuthorizationHeaderNew())
      .map((tags: Tag[]) => {
        this.tags = tags;
        this.sortByName();
        this._tagsBehavior.next(this.tags);
        return this.tags;
      })
      .pipe(catchError(this.handleError));
  }

  private sortByName(): void {
    this.tags.sort((tag1, tag2) => {
      if (tag1.name > tag2.name) {
        return 1;
      }

      if (tag1.name < tag2.name) {
        return -1;
      }
      return 0;
    });
  }

  public add(tag: Tag): Observable<Tag> {
    return this.http.post(environment.API_URL + '/api/tags/', tag,
      this.getAuthorizationHeaderNew())
      .map((tag: Tag) => {
        return tag;
      })
      .pipe(catchError(this.handleError));
  }

  public update(tag: Tag): Observable<Tag> {
    return this.http.put(environment.API_URL + '/api/tags/' + tag.id + '/', tag,
      this.getAuthorizationHeaderNew())
      .map((tag: Tag) => {
        this._tagsBehavior.next(this.tags);
        return TagService.createTag(tag.id, tag.name);
      })
      .pipe(catchError(this.handleError));
  }

  public remove(tag: Tag): Observable<any> {
    this.tags.splice(this.tags.indexOf(tag), 1);
    this._tagsBehavior.next(this.tags);
    return this.http.delete(environment.API_URL + '/api/tags/' + tag.id + '/',
      this.getAuthorizationHeaderNew())
      .map(data => {
        this._tagsBehavior.next(this.tags);
        this.logger.debug(`Tag '${tag.name}' was deleted`);
        return data;
      })
      .pipe(catchError(this.handleError));
  }

  private static createTag(id: number, name: string): Tag {
    return new Tag(id, name);
  }

  get tagsBehaviour(): Observable<Tag[]> {
    return this._tagsBehavior.asObservable();
  }
}
