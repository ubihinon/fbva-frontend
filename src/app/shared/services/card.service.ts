import { Inject, Injectable } from '@angular/core';
import { Card } from '../model/Card';
import { AuthService } from './auth.service';
import { Word } from '../model/Word';
import { Tag } from '../model/Tag';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';
import { Language } from '../model/Language';
import { NGXLogger } from 'ngx-logger';
import { BehaviorSubject } from 'rxjs';
import { IntervalCard } from '../model/IntervalCard';
import { environment } from "../../../environments/environment";
import { PronunciationService } from "./pronunciation.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class CardService extends BaseService {
  public cards: Card[] = [];
  private _cardsToLearn: Card[] = [];
  private _allCardsToLearn: Card[] = [];
  private _cardsBehaviour: BehaviorSubject<Card[]>;
  public cardsToLearnBehaviour: BehaviorSubject<Card[]>;

    constructor(private http: HttpClient,
                private pronunciationService: PronunciationService,
                @Inject(AuthService) authService: AuthService,
                public logger: NGXLogger) {
        super(authService);
        this._cardsBehaviour = new BehaviorSubject(this.cards);
        this.cardsToLearnBehaviour = new BehaviorSubject(this._cardsToLearn);
    }

  public create(word: Word,
                tags: Tag[],
                sourceLanguage?: Language,
                targetLanguage?: Language): Observable<Card[]> {
    let card = new Card(
      null,
      0,
      'no',
      word,
      null,
      null,
      tags,
      sourceLanguage,
      targetLanguage
    );
    let headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + JSON.parse(localStorage.getItem('currentUserToken')).token
      })
    };
    console.log('CARD JSON:', JSON.stringify(card));
    return this.http.post<Card>(environment.API_URL + '/api/cards/create/', card, headers)
      .map(
        newCard => {
          card = newCard;
          this.logger.debug(`Add card ${card.id}`);
          this.cards.forEach(card => console.log(`CARD: ${card.word.word}`));
          this.cards.push(card);
          this._cardsBehaviour.next(this.cards);
          return this.cards;
        }
      ).pipe(catchError(this.handleError));
  }

  public update(card: Card): Observable<Card> {
    return this.http.put<Card>(environment.API_URL + '/api/cards/' + card.id + '/update/', card,
      this.getAuthorizationHeaderNew())
      .map(
        (result) => {
          this.logger.debug(`Update card ${JSON.stringify(result)}`);
          return result;
        },
        (result) => {
          this.logger.debug(`Failed to update card ${JSON.stringify(result)}`);
          return result;
        }
      ).pipe(catchError(this.handleError))
  }

  public updateInterval(card: Card): Observable<IntervalCard> {
    return this.http.put<IntervalCard>(
      environment.API_URL + '/api/cards/' + card.id + '/interval/update/', card,
      this.getAuthorizationHeaderNew()
    )
      .map(
        (result) => {
          this.logger.debug(`Update interval of card ${JSON.stringify(result)}`);
          return result;
        },
        (result) => {
          this.logger.debug(`Failed to update interval of card ${JSON.stringify(result)}`);
          return result;
        }
      ).pipe(catchError(this.handleError))
  }

  public getAll(): Observable<Card[]> {
    this.cards = [];
    return this.http.get<Card[]>(environment.API_URL + '/api/cards/')
      .map(cards => {
        this.logger.debug(`Loaded all cards`);
        for (let tempCard of cards) {
          let card = <Card> tempCard;
          this.cards.push(card);
        }
        return this.cards;
      }).pipe(catchError(this.handleError))
  }

  public loadAllCardsForCurrentUser(): Observable<Card[]> {
    return this.http.get<Card[]>(
      environment.API_URL + '/api/cards/?user_id=' + this.authService.getUser().id,
      this.getAuthorizationHeaderNew()
    )
      .map(cards => {
        this.logger.debug(`Loaded all cards for current user`);
        this.cards = [];
        for (let tempCard of cards) {
          let card = <Card> tempCard;
          this.cards.push(card);
        }
        return this.cards;
      }).pipe(catchError(this.handleError))
  }

  public getByUserIdForRevise(): Observable<Card[]> {
    return this.http.get<Card[]>(
      environment.API_URL + '/api/cards/?user_id=' + this.authService.getUser().id
      + '&words_to_learn=true',
      this.getAuthorizationHeaderNew()
    )
      .map(cards => {
        this.logger.debug(`Loaded ${cards.length} cards for current user for revising`);
        this._allCardsToLearn = [];
        for (let tempCard of cards) {
          let card = <Card> tempCard;
          this._allCardsToLearn.push(card);
        }
        return this._allCardsToLearn;
      }).pipe(catchError(this.handleError))
  }

  public getCardsByTag(tag: Tag): Card[] {
    let cards = [];
    for (let card of this.cards) {
      if (card.tags != null) {
        for (let currentTag of card.tags) {
          if (tag.id == currentTag.id) {
            cards.push(card);
          }
        }
      }
    }
    this.cardsToLearnBehaviour.next(cards);
    return cards;
  }

  public deleteById(card: Card): Observable<{}> {
    this.logger.debug(`Delete card ${JSON.stringify(card)}`);
    return this.http.delete(
      environment.API_URL + '/api/cards/' + card.id + '/delete/',
      this.getAuthorizationHeaderNew()).pipe(catchError(this.handleError));
  }

  public deleteByIdList(cards: Card[]): Observable<{}> {
    this.logger.debug(`Delete card list`);

    let idList = '';
    for (let card of cards) {
      idList += card.id + ','
    }
    return this.http.get(environment.API_URL + '/api/cards/delete/?id_list=' + idList,
      this.getAuthorizationHeaderNew()).pipe(catchError(this.handleError));
  }

  copyAllCardsToCardsToLearn(): void {
    this._cardsToLearn = this._allCardsToLearn;
  }

  set cardsToLearn(value: Card[]) {
    this._cardsToLearn = value;
    this.cardsToLearnBehaviour.next(this._cardsToLearn);
  }

  get cardsToLearn(): Card[] {
    return this._cardsToLearn;
  }

  get allCardsToLearn(): Card[] {
    return this._allCardsToLearn;
  }

  get cardsBehaviour(): Observable<Card[]> {
    return this._cardsBehaviour.asObservable();
  }
}
