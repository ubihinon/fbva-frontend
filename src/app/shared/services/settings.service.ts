import { Inject, Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Setting } from '../model/Setting';
import { AuthService } from './auth.service';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {catchError} from "rxjs/operators";

@Injectable()
export class SettingsService extends BaseService {
  settings: Map<string, Setting> = new Map();

  constructor(public logger: NGXLogger,
              private http: HttpClient,
              @Inject(AuthService) authService: AuthService) {
    super(authService);
  }

  public save(name: string, value: string): void {
    let userSetting = new Setting(name, value);
    this.http.post<Setting>(environment.API_URL + '/api/settings/user-settings/',
      userSetting, this.getAuthorizationHeaderNew())
      .map(userSetting => {
        this.logger.debug(`Setting ${userSetting.name} = ${userSetting.value} saved`);
        this.settings.set(userSetting.name, <Setting> userSetting);
      }).pipe(catchError(this.handleError))
      .subscribe()
  }

  public loadAllForCurrentUser(): Observable<Setting[]> {
    return this.http.get<Setting[]>(environment.API_URL + '/api/settings/user-settings/',
      this.getAuthorizationHeaderNew())
      .map(settings => {
        this.settings.clear();
        for (let tempSetting of settings) {
          let setting = <Setting> tempSetting;
          this.settings.set(setting.name, setting);
          this.logger.debug(`Setting ${setting.name} = ${setting.value} loaded`);
        }
        // return this.settings;
        return settings
      }).pipe(catchError(this.handleError))
  }

  public getValue(name: string): string {
    return this.settings.get(name) ? this.settings.get(name).value : null;
  }
}
