import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {RequestOptions, Headers} from '@angular/http';
import {NGXLogger} from 'ngx-logger';
import {Observable} from 'rxjs';
import {HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {throwError} from "rxjs/internal/observable/throwError";

@Injectable()
export class BaseService {
    constructor(public authService?: AuthService) {
    }

    protected getAuthorizationHeader(): RequestOptions {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // headers.append('Authorization', 'JWT ' + this.authService.token);
        headers.append(
            'Authorization',
            'JWT ' + JSON.parse(localStorage.getItem('currentUserToken')).token
        );
        return new RequestOptions({headers: headers});
    }

    protected getAuthorizationHeaderNew(): {} {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'JWT ' +
                JSON.parse(localStorage.getItem('currentUserToken')).token
            })
        };
    }
    protected getJSONContentTypeHeaderNew(): {} {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }
    protected getJSONContentTypeHeader(): RequestOptions {
        let headers = new Headers({'Content-Type': 'application/json'});
        return new RequestOptions({headers: headers});
    }

    public handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    };
}
