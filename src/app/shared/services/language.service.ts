import { Injectable } from '@angular/core';
import { Language } from '../model/Language';

import { Observable } from 'rxjs/';
import { catchError } from 'rxjs/operators';
import { BaseService } from './base.service';
import { NGXLogger } from 'ngx-logger';
import { environment } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class LanguageService extends BaseService {
  languages: Language[] = [];

  constructor(private http: HttpClient,
              public logger: NGXLogger) {
    super();
  }

  public getAll(): Observable<Language[]> {
    this.logger.debug(`Get all languages`);

    this.languages = [];
    return this.http.get<Language[]>(environment.API_URL + '/api/languages/')
      .map(languages => {
        for (let tempLanguage of languages) {
          let language = <Language> tempLanguage;
          this.languages.push(language);
        }
        return this.languages;
      }).pipe(catchError(this.handleError))
  }

  public getById(language_to_find: number): Language {
    this.logger.debug(`Get language by id`);
    let foundLanguage: Language = null;
    for (let language of this.languages) {
      if (language.id == language_to_find) {
        foundLanguage = language;
        break;
      }
    }
    return foundLanguage;
  }
}
