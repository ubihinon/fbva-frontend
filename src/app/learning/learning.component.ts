import { Component, OnDestroy, OnInit } from '@angular/core';
import { Card } from '../shared/model/Card';
import { CardService } from '../shared/services/card.service';
import { LearningSession } from '../shared/model/LearningSession';
import { NGXLogger } from 'ngx-logger';
import { IntervalCard } from '../shared/model/IntervalCard';
import { LinkedList } from '../shared/model/LinkedList';
import { SettingName } from "../shared/constants/SettingName";
import { SettingsService } from "../shared/services/settings.service";

@Component({
  selector: 'app-learning',
  templateUrl: './learning.component.html',
  styleUrls: ['./learning.component.scss']
})
export class LearningComponent implements OnInit, OnDestroy {
  public sessions: LinkedList<LearningSession> = new LinkedList();
  public currentSession: LearningSession = null;
  protected showTranslations: boolean = false;
  protected disableButtons: boolean = false;
  private COUNT_SHOWS: number = 3;
  private COUNT_SESSION_CARDS: number = 3;
  private audio = new Audio();

  constructor(private cardService: CardService,
              private settingsService: SettingsService,
              private logger: NGXLogger) {
  }

  ngOnInit() {
    this.cardService.cardsToLearnBehaviour.subscribe(() => {
      this.init();
    });
  }

  ngOnDestroy(): void {
      this.audio = null;
  }

  protected init(): void {
    this.sortByLastShown();
    this.sortByNew();
    this.sessions.clear();
    this.fillSessions(this.cardService.cardsToLearn.slice(0, this.COUNT_SESSION_CARDS));
    this.currentSession = this.sessions.getValue();
    this.playPronunciation();
  }

  private fillSessions(cards: Card[]): void {
    for (let card of cards) {
      this.sessions.push(new LearningSession(card));
    }
  }

  private sortByLastShown(): void {
    this.cardService.cardsToLearn.sort((card1, card2) => {
      let date1 = new Date(card1.intervalCard.lastShown);
      let date2 = new Date(card2.intervalCard.lastShown);
      if (date1.getTime() > date2.getTime()) {
        return 1;
      }
      if (date1.getTime() < date2.getTime()) {
        return -1;
      }
      return 0;
    });
  }

  private sortByNew(): void {
    this.cardService.cardsToLearn.sort((card1, card2) => {
      let date1 = new Date(card1.intervalCard.lastShown);
      let date2 = new Date(card2.intervalCard.lastShown);
      if (date1.getTime() == 0) {
        return 1;
      }
      if (date2.getTime() == 0) {
        return 1;
      }
      return -1;
    });
  }

  public isSessionFinished(): boolean {
    let isNotFinished = true;
    for (let session of this.sessions) {
      if (session.getCountRightAnswers() < this.COUNT_SHOWS) {
        isNotFinished = false;
        break;
      }
    }
    return isNotFinished;
  }

  protected isOneSessionLeft(): boolean {
    let countSessionLeft = 0;
    for (let session of this.sessions) {
      if (session.getCountRightAnswers() < this.COUNT_SHOWS) {
        countSessionLeft++;
      }
    }
    return countSessionLeft == 1
      && this.currentSession.getCountRightAnswers() > 1
      && this.currentSession.getCountRightAnswers() < this.COUNT_SHOWS;
  }

  private nextCard(): void {
    if (this.sessions.getValue().getCountRightAnswers() >= this.COUNT_SHOWS
      && !this.sessions.getValue().wroteToDB) {

      this.sessions.getValue().wroteToDB = true;
      this.currentSession.card.intervalCard.lastShown = new Date().toISOString();
      if (this.currentSession != null) {
        this.deleteCurrentCardFromCardsToLearn();
        this.cardService.updateInterval(this.currentSession.card)
          .subscribe(intervalCard => {
            this.currentSession.card.intervalCard = <IntervalCard> intervalCard;
            if (this.isSessionFinished()) {
              this.currentSession = null;
              return;
            }
          });
      }
    }

    this.sessions.next();
    if (this.sessions.getValue().getCountRightAnswers() < this.COUNT_SHOWS) {
      this.currentSession = this.sessions.getValue();
      this.playPronunciation();
    }
  }

  protected playPronunciation(play_anyway = false): void {
    if ((this.settingsService.getValue(SettingName.AUTO_PRONUNCIATION.toString()) == 'true'
      || play_anyway)
      && this.audio
    ) {
      this.audio.src = this.currentSession.card.pronunciationPath;
      this.audio.load();
      this.audio.play();
    }
  }

  private deleteCurrentCardFromCardsToLearn(): void {
    this.cardService.cardsToLearn.splice(
      this.cardService.cardsToLearn.indexOf(this.currentSession.card),
      1
    );
  }

  protected onKnow(): void {
    this.showTranslations = true;
    this.disableButtons = true;
    this.currentSession.increaseCountRightAnswers();
    this.logger.debug(
      `
      Button 'I Know' pressed.
      Word: ${this.currentSession.card.word.word}.
      Right count answers in a row: ${this.currentSession.getCountRightAnswers()}`
    );
    setTimeout(() => this.next(), 1000);
  }

  protected onDoNotKnow(): void {
    this.showTranslations = true;
    this.disableButtons = true;
    this.currentSession.resetCountRightAnswers();
    this.logger.debug(
      `
      Button 'I don't Know' pressed,
      Word: ${this.currentSession.card.word.word},
      Right count answers in a row: ${this.currentSession.getCountRightAnswers()},`
    );
    setTimeout(() => this.next(), 2000);
  }

  private next(): void {
    this.showTranslations = false;
    this.nextCard();
    this.disableButtons = false;
  }

  protected getTranslationsString(): string {
    let translations = '';
    for (let translation of this.currentSession.card.word.translations) {
      translations += `${translation.text}, `
    }
    return translations.slice(0, -2);
  }
}
