import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NoContentComponent } from './no-content';
import { StartComponent } from './start/start.component';
import { AuthGuard } from './guards/auth.guard';
import { SettingsComponent } from './settings/settings.component';
import { AddingCardComponent } from './adding-card/adding-card.component';
import { CardListComponent } from './card-list/card-list.component';
import { LearningComponent } from './learning/learning.component';

export const ROUTES: Routes = [
  {path: 'start', component: StartComponent},
  {
    path: '', component: HomeComponent, canActivate: [AuthGuard], children: [
      {path: '', redirectTo: 'adding-card', pathMatch: 'full'},
      {path: 'settings', component: SettingsComponent},
      {path: 'adding-card', component: AddingCardComponent},
      {path: 'card-list', component: CardListComponent},
      {path: 'learning', component: LearningComponent}
    ]
  },
  {path: '**', component: NoContentComponent}
];
