import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { CardService } from '../shared/services/card.service';
import { Card } from '../shared/model/Card';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { CardEditDialogComponent } from './card-edit-dialog/card-edit-dialog.component';
import { TagService } from '../shared/services/tag.service';
import { Observable } from 'rxjs';
import { Tag } from '../shared/model/Tag';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit, AfterViewInit {
  private allCards = [];
  private selectedCards: Card[] = [];
  public cards = [];
  public tags: Observable<Array<Tag>> | null = null;
  cardFilter: string = '';
  @ViewChild('inputCardFilter') inputCardFilter: ElementRef;

  constructor(private cardService: CardService,
              private tagService: TagService,
              private logger: NGXLogger,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.tags = this.tagService.tagsBehaviour;

    this.tags.subscribe(tags => {
      this.cardService.loadAllCardsForCurrentUser().subscribe(cards => {
        this.allCards = [];
        this.cards = [];
        this.allCards = Array.from(new Set(cards));
        this.cards = Array.from(new Set(cards));
      });
    });

    this.tagService.selectedTagEvent.subscribe(selectedTag => {
      this.cards = [];
      this.allCards.forEach(card => {
        this.cardFilter = '';
        for (let tag of card.tags) {
          if (selectedTag.id == tag.id) {
            this.cards.push(card);
            break;
          }
        }
      });
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.setFocusInputCardFilter(), 100);
  }

  private setFocusInputCardFilter(): void {
    if (this.inputCardFilter) {
      this.inputCardFilter.nativeElement.focus()
    }
  }

  protected onRowClickEvent(card: Card, event): void {
    if (!event.ctrlKey && !event.shiftKey) {
      this.selectedCards = [];
      this.selectedCards.push(card);
      this.openDialog(card);
    } else if (event.ctrlKey) {
      this.toggleItemInArray(this.selectedCards, card);
    } else if (event.shiftKey) {
      this.selectPartCards(card);
    }
  }

  protected addSelectedCard(card: Card): void {
    if (this.selectedCards.length == 0) {
      this.selectedCards.push(card);
    }
  }

  private selectPartCards(card: Card): void {
    if (this.isCardSelected(card)) {
      this.toggleItemInArray(this.selectedCards, card);
    } else if (this.selectedCards.length == 0) {
      this.selectedCards.push(card);
    } else {
      let startIndex = this.cards.indexOf(this.selectedCards[0]);
      let endIndex = this.cards.indexOf(card);

      for (let currentCard of this.cards) {
        let currentIndex = this.cards.indexOf(currentCard);
        if (startIndex < endIndex) {
          if (currentIndex > startIndex && currentIndex <= endIndex) {
            this.selectedCards.push(currentCard);
          }
        } else {
          if (currentIndex < startIndex && currentIndex >= endIndex) {
            this.selectedCards.push(currentCard);
          }
        }
      }
    }
  }

  private openDialog(card: Card): void {
    this.logger.debug(`Open edit dialog`);
    let dialogRef = this.dialog.open(CardEditDialogComponent, <MatDialogConfig> {
      panelClass: 'centered-dialog'
    });
    dialogRef.componentInstance.setCard(card);
    dialogRef.afterClosed().subscribe(deletedCard => {
      let index = this.cards.indexOf(deletedCard);
      if (index != -1) {
        this.cards.splice(index, 1);
      }
    });
  }

  private toggleItemInArray(array: Card[], item: any): void {
    const index = array.indexOf(item);
    index === -1 ? array.push(item) : array.splice(index, 1);
  }

  private isCardSelected(card: Card): boolean {
    return this.selectedCards.indexOf(card) !== -1;
  }

  protected deleteSelectedCards(): void {
    this.logger.debug(`Delete selected cards`);
    this.cardService.deleteByIdList(this.selectedCards).subscribe();
    for (let card of this.selectedCards) {
      this.elementInArray(this.cards, card) ? this.cards.splice(this.cards.indexOf(card), 1) : '';
    }
    this.selectedCards.splice(0, this.selectedCards.length);
  }

  private elementInArray(array: Card[], item: any): boolean {
    return array.indexOf(item) !== -1;
  }

  protected joinCardArray(card: Card, type: string): string {
    let joinedArray = '';
    if (type == 'translations') {
      for (let translation of card.word.translations) {
        joinedArray += translation.text;
        joinedArray += ' '
      }
    } else if (type == 'tags') {
      for (let tag of card.tags) {
        joinedArray += tag.name;
        joinedArray += ' '
      }
    }
    return joinedArray
  }
}
