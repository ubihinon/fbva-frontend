import { Component, OnInit, OnChanges, SimpleChanges, AfterViewInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Card } from '../../shared/model/Card';
import { Tag } from '../../shared/model/Tag';
import { YandexSearchService } from '../../shared/services/yandexsearch.service';
import { Translation } from '../../shared/model/Translation';
import { CardService } from '../../shared/services/card.service';
import { NGXLogger } from 'ngx-logger';
import { LanguageService } from '../../shared/services/language.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: 'card-edit-dialog.component.html',
  styleUrls: ['card-edit-dialog.component.scss']
})
export class CardEditDialogComponent implements OnInit, OnChanges, AfterViewInit {
  title: string;
  cardToEdit: Card = new Card();
  card: Card;
  public translations: Translation[];
  private selectedTranslations: Translation[];

  constructor(public dialogRef: MatDialogRef<CardEditDialogComponent>,
              private yandexSearchService: YandexSearchService,
              private languageService: LanguageService,
              private cardService: CardService,
              private logger: NGXLogger) {
    this.cardToEdit = new Card();
  }

  ngOnInit() {
    this.translations = [];
    this.selectedTranslations = [];
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.translations = [];
  }

  ngAfterViewInit(): void {
    this.translations = [];
    this.searchWord();
  }

  public searchWord(): void {
    this.yandexSearchService.getTranslationsByWordName(
      this.cardToEdit.word.word,
      this.card.sourceLanguage,
      this.card.targetLanguage
    )
      .subscribe(translations => {
        this.translations = translations;
      });
  }

  public setCard(value: Card) {
    this.card = value;
    this.cardToEdit = Card.clone(this.card);
  }

  public saveChanges(): void {
    if (this.cardToEdit.word.word != '' && this.cardToEdit.word.translations.length > 0) {
      this.card.word.word = this.cardToEdit.word.word;
      this.card.word.translations = this.cardToEdit.word.translations;
      this.card.tags = this.cardToEdit.tags;
      this.cardService.update(this.cardToEdit).subscribe();
    }
  }

  public addSelectedTranslations(translations: Translation[]): void {
    this.cardToEdit.word.translations = this.cardToEdit.word.translations.concat(translations);
  }

  public addCustomTranslations(translations: Translation[]) {
    this.cardToEdit.word.translations = translations;
  }

  public addTags(tags: Tag[]): void {
    this.cardToEdit.tags = [];
    this.cardToEdit.tags = tags;
  }

  public deleteCard(): void {
    this.cardService.deleteById(this.card).subscribe(res => {
      this.logger.debug(`Status of deletion card: ${res}`)
    });
  }
}
