import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserModule} from '@angular/platform-browser';
import {ROUTES} from "./app.routes";
import '../assets/styles/styles.scss';
import '../assets/favicons/favicons.js';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TopMenuBarComponent} from "./top-menu-bar/top-menu-bar.component";
import {SearchFilterPipe} from "./shared/pipes/SearchFilterPipe";
import {SideMenuComponent} from "./side-menu/side-menu.component";
import {
    DateAdapter, MAT_DATE_FORMATS,
    MatAutocompleteModule, MatButtonModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatIconModule,
    MatMenuModule, MatNativeDateModule, NativeDateAdapter
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
    RouterModule,
    PreloadAllModules
} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {StartComponent} from "./start/start.component";
import {TagListComponent} from "./tag-list/tag-list.component";
import {AddingCardComponent} from "./adding-card/adding-card.component";
import {TagsComponent} from "./adding-card/tags/tags.component";
import {AuthComponent} from "./auth/auth.component";
import {CardEditDialogComponent} from "./card-list/card-edit-dialog/card-edit-dialog.component";
import {CardListComponent} from "./card-list/card-list.component";
import {CustomTranslationsComponent} from "./cards/custom-translations/custom-translations.component";
import {InternetTranslationsComponent} from "./cards/internet-translations/internet-translations.component";
import {LearningComponent} from "./learning/learning.component";
import {NoContentComponent} from "./no-content";
import {SettingsComponent} from "./settings/settings.component";
import {RegistryComponent} from "./registry/registry.component";
import {SettingsGeneralTabComponent} from "./settings/settings-general-tab/settings-general-tab.component";
import {SettingsPrivacyTabComponent} from "./settings/settings-privacy-tab/settings-privacy-tab.component";
import {ContextMenuModule} from "ngx-contextmenu";
import {LoggerModule, NgxLoggerLevel} from "ngx-logger";
import {TagInputModule} from "ngx-chips";
import {AuthGuard} from "./guards/auth.guard";
import {AuthService} from "./shared/services/auth.service";
import {UserService} from "./shared/services/user.service";
import {LanguageService} from "./shared/services/language.service";
import {CardService} from "./shared/services/card.service";
import {TagService} from "./shared/services/tag.service";
import {SettingsService} from "./shared/services/settings.service";
import {YandexSearchService} from "./shared/services/yandexsearch.service";
import {HttpClientModule} from '@angular/common/http';
import {MaterializeModule} from "angular2-materialize";
import 'materialize-css';
import {PronunciationService} from "./shared/services/pronunciation.service";
import { SettingsOtherTabComponent } from './settings/settings-other-tab/settings-other-tab.component';

@NgModule({
    bootstrap: [AppComponent],
    entryComponents: [CardEditDialogComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        MatDialogModule,
        MatChipsModule,
        MaterializeModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatNativeDateModule,
        TagInputModule,
        ContextMenuModule.forRoot(),
        LoggerModule.forRoot(
            {
                serverLoggingUrl: '/api/logs',
                level: NgxLoggerLevel.DEBUG,
                serverLogLevel: NgxLoggerLevel.ERROR
            }
        ),
        RouterModule.forRoot(ROUTES, {preloadingStrategy: PreloadAllModules})
    ],
    declarations: [
        AppComponent,
        TopMenuBarComponent,
        HomeComponent,
        StartComponent,
        AuthComponent,
        SideMenuComponent,
        TagListComponent,
        TagsComponent,
        CardListComponent,
        AddingCardComponent,
        CardEditDialogComponent,
        InternetTranslationsComponent,
        CustomTranslationsComponent,
        LearningComponent,
        NoContentComponent,
        SettingsComponent,
        SettingsGeneralTabComponent,
        SettingsPrivacyTabComponent,
        RegistryComponent,
        SearchFilterPipe,
        SettingsOtherTabComponent,
    ],
    providers: [
        AuthGuard,
        AuthService,
        UserService,
        LanguageService,
        CardService,
        TagService,
        SettingsService,
        YandexSearchService,
        PronunciationService,
        {provide: DateAdapter, useClass: NativeDateAdapter},
        {provide: MAT_DATE_FORMATS, useValue: 'yyyy-mm-dd'},
    ]
})
export class AppModule {
}