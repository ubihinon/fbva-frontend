import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { User } from '../shared/model/User';
import { CardService } from '../shared/services/card.service';
import * as $ from 'jquery';

@Component({
  selector: 'top-menu-bar',
  templateUrl: './top-menu-bar.component.html',
  styleUrls: ['./top-menu-bar.component.scss']
})
export class TopMenuBarComponent implements OnInit, AfterViewInit {

  currentUser: User;

  constructor(public authService: AuthService,
              protected cardService: CardService) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    $('.button-collapse').sideNav();
  }

  protected logout(): void {
    this.authService.logout();
  }

  protected copyAllCardsToCardToLearn(): void {
    this.cardService.copyAllCardsToCardsToLearn();
  }
}
