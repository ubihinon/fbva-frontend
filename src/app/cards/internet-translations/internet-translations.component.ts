import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter
} from '@angular/core';
import { Translation } from '../../shared/model/Translation';

@Component({
  selector: 'app-internet-translations',
  templateUrl: './internet-translations.component.html',
  styleUrls: ['./internet-translations.component.scss']
})
export class InternetTranslationsComponent implements OnInit, OnChanges {
  @Input() public wordName: string = '';
  @Input() private excludedTranslations: Translation[] = [];
  @Input() private allTranslations: Translation[] = [];
  public translations: Translation[] = [];
  @Output() selectedTranslationsEvent = new EventEmitter<Translation[]>();
  private selectedTranslations: Translation[] = [];

  constructor() {
  }

  ngOnInit() {
    this.translations = [];
    this.selectedTranslations = [];
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.selectedTranslations = [];
    this.translations = [];
    this.excludeContainingTranslations();
  }

  protected addSelectedTranslation(translation: Translation): void {
    let index = this.selectedTranslations.indexOf(translation);
    if (index == -1) {
      this.selectedTranslations.push(translation);
    } else {
      this.selectedTranslations.splice(index, 1);
    }
    this.selectedTranslationsEvent.emit(this.selectedTranslations);
  }

  private excludeContainingTranslations(): void {
    this.translations = this.translations.concat(this.allTranslations);
    for (let excludeTranslation of this.excludedTranslations) {
      for (let translation of this.translations) {
        if (excludeTranslation.text == translation.text) {
          this.translations.splice(this.translations.indexOf(translation), 1);
        }
      }
    }
  }
}
