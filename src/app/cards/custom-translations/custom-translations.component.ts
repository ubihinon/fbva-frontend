import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { Translation } from '../../shared/model/Translation';

@Component({
  selector: 'app-custom-translations',
  templateUrl: './custom-translations.component.html',
  styleUrls: ['./custom-translations.component.scss']
})
export class CustomTranslationsComponent implements OnInit, OnChanges {
  @Input() wordName: string = '';
  translations: Translation[] = [];
  @Input() inputTranslations: Translation[];
  @Output() addedCustomTranslationsEvent = new EventEmitter<Translation[]>();

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initTranslations();
  }

  private initTranslations(): void {
    this.translations = [];
    if (this.inputTranslations != null) {
      for (let tr of this.inputTranslations) {
        this.translations.push(tr);
      }
    }
    if (!this.emptyTranslationIsExists()) {
      this.translations.push(new Translation('', ''));
    }
  }

  protected addTranslation(event: any, translation: Translation): void {
    if (event.target.value != '') {
      translation.text = event.target.value;
      if (!this.emptyTranslationIsExists()) {
        this.translations.push(new Translation('', ''));
      }
    } else {
      this.deleteField(translation)
    }
    this.translationsChanged();
  }

  private emptyTranslationIsExists(): boolean {
    for (let translation of this.translations) {
      if (translation.text.trim() == '') {
        return true;
      }
    }
    return false;
  }

  private deleteField(translation: Translation): void {
    this.translations.splice(this.translations.indexOf(translation), 1);
  }

  protected deleteNotEmptyField(translation: Translation): void {
    if (this.translations.length > 1) {
      if (translation.text != '') {
        this.deleteField(translation);
      }
      this.translationsChanged();
    }
  }

  private translationsChanged(): void {
    this.addedCustomTranslationsEvent.emit(this.getNotEmptyTranslations());
  }

  private getNotEmptyTranslations(): Translation[] {
    let notEmptyTranslations = [];
    for (let translation of this.translations) {
      if (translation.text.trim() != '') {
        notEmptyTranslations.push(translation);
      }
    }
    return notEmptyTranslations;
  }

}
