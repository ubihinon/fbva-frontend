import { Component, OnInit } from '@angular/core';
import { User } from '../shared/model/User';
import { TagService } from '../shared/services/tag.service';
import { SettingsService } from '../shared/services/settings.service';
import { CardService } from '../shared/services/card.service';

@Component({
  moduleId: 'module.id',
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})
export class HomeComponent implements OnInit {
  currentUser: User;

  constructor(private tagService: TagService,
              private settingsService: SettingsService,
              private cardService: CardService) {
  }

  ngOnInit(): void {
    this.tagService.loadTagsForCurrentUser().subscribe();
    this.settingsService.loadAllForCurrentUser().subscribe();
    this.cardService.getByUserIdForRevise().subscribe();
    // this.cardService.loadAllCardsForCurrentUser().subscribe();
    setInterval(() => {
      this.cardService.getByUserIdForRevise().subscribe();
    }, 30000)
  }


}
