import { Component, OnInit } from '@angular/core';
import { SettingsService } from "../../shared/services/settings.service";
import { SettingName } from "../../shared/constants/SettingName";
import { NGXLogger } from "ngx-logger";

@Component({
  selector: 'app-settings-other-tab',
  templateUrl: './settings-other-tab.component.html',
  styleUrls: ['./settings-other-tab.component.scss', '../settings.component.scss']
})
export class SettingsOtherTabComponent implements OnInit {
  public pronunciation: boolean;

  constructor(public logger: NGXLogger, private settingsService: SettingsService) {
  }

  ngOnInit() {
    this.pronunciation = this.settingsService.getValue(
      SettingName.AUTO_PRONUNCIATION.toString()) == 'true';
    console.log(`ngOnInit: ${this.pronunciation}`)
  }

  public saveOther(): void {
    console.log('saveOther: ' + this.pronunciation);
    this.logger.debug(`Auto pronunciation changed to ${this.pronunciation}`);
    this.settingsService.save(
      SettingName.AUTO_PRONUNCIATION.toString(), JSON.stringify(this.pronunciation)
    );
  }

}
