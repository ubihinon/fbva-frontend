import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { AuthService } from '../../shared/services/auth.service';
import { User } from '../../shared/model/User';
import { NGXLogger } from 'ngx-logger';
import { catchError } from "rxjs/operators";

@Component({
  selector: 'settings-privacy',
  templateUrl: './settings-privacy-tab.component.html',
  styleUrls: ['./settings-privacy-tab.component.scss', '../settings.component.scss']
})
export class SettingsPrivacyTabComponent implements OnInit {

  currentPassword: string = '';
  newPassword1: string = '';
  newPassword2: string = '';

  currentPasswordError: string = '';
  password1Error: string = '';
  password2Error: string = '';

  constructor(private userService: UserService,
              private authService: AuthService,
              private logger: NGXLogger) {
  }

  ngOnInit(): void {
  }

  public savePassword(): void {
    this.currentPasswordError = this.password1Error = this.password2Error = '';
    if (this.checkPasswords()) {
      this.userService.changePassword(this.currentPassword, this.newPassword1,
        this.authService.getUser())
        .subscribe(() => {
          this.logger.debug('New password saved successfully');
          this.currentPasswordError = '';
          this.clearPasswordFields();
          // Materialize.toast('You\'ve changed your password successfully', 4000);
        })
        // .pipe(catchError(this.handleError));
        // .catch(error => {
        //   this.currentPasswordError = 'Wrong current password';
        //   this.logger.error(`While saving password the error occurred: ${error}`);
        //   // Materialize.toast(
        //   //   'While saving password the error occurred: ' + error,
        //   //   4000
        //   // );
        // });
    }
  }

  private checkPasswords(): boolean {
    if (this.currentPassword.length == 0 || this.currentPassword == null) {
      this.currentPasswordError = 'This field mustn\'t be empty';
    }
    if (this.newPassword1.length == 0 && this.newPassword2.length == 0) {
      this.password1Error = this.password2Error = 'This field mustn\'t be empty';
    }
    if (this.newPassword1 != this.newPassword2) {
      this.password2Error = 'Passwords don\'t match';
    }
    if (!User.passwordIsRight(this.newPassword1) && this.newPassword1.length > 0) {
      this.password1Error = 'Password must have minimum 8 and maximum 128 characters ' +
        'and must contain of numbers and letters';
    }
    return this.currentPasswordError.length == 0
      && this.password1Error.length == 0
      && this.password2Error.length == 0;
  }

  private clearPasswordFields(): void {
    this.currentPassword = this.newPassword1 = this.newPassword2 = '';
    let labels = document.getElementsByTagName('label');
    for (let i: number = 0; i < labels.length; i++) {
      if (labels[i].getAttribute('for') == 'current_password'
        || labels[i].getAttribute('for') == 'new_password1'
        || labels[i].getAttribute('for') == 'new_password2') {
        labels[i].classList.remove('active');
      }
    }
  }
}
