///<reference path="../../../node_modules/@angular/platform-browser/src/browser/title.d.ts"/>
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import * as $ from 'jquery';

@Component({
  moduleId: 'module.id',
  selector: 'settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.scss'],
})
export class SettingsComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle('Settings');
  }

  ngOnInit(): void {
    $(document).ready(function () {
      $('ul.tabs').tabs();
    });
  }
}
