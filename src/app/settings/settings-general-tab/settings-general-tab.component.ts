import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/model/User';
import { Language } from '../../shared/model/Language';
import { UserService } from '../../shared/services/user.service';
import { AuthService } from '../../shared/services/auth.service';
import { LanguageService } from '../../shared/services/language.service';
import { NGXLogger } from 'ngx-logger';
import * as $ from 'jquery';

@Component({
  selector: 'settings-general',
  templateUrl: 'settings-general-tab.component.html',
  styleUrls: ['settings-general-tab.component.scss', '../settings.component.scss']
})
export class SettingsGeneralTabComponent implements OnInit {

  sexList = [
    {value: 'M', viewValue: 'Male'},
    {value: 'F', viewValue: 'Female'}
  ];

  username: string = '';
  email: string = '';
  sex: string = 'M';
  birthday: Date = null;
  usernameError: string = '';
  emailError: string = '';
  languages: Language[];
  selectedLanguage: Language;

  constructor(private userService: UserService,
              private authService: AuthService,
              private languageService: LanguageService,
              private logger: NGXLogger) {
  }

  ngOnInit(): void {
    $(document).ready(function () {
      $('select').material_select();
    });

    $('.datepicker').pickadate({
      selectMonths: true,
      selectYears: 150,
      today: '',
      format: 'yyyy-mm-dd',
      clear: 'Clear',
      close: 'Ok',
      closeOnSelect: false,
      min: new Date('1900-01-01'),
      max: new Date()
    });

    this.username = this.authService.currentUser.username;
    this.email = this.authService.currentUser.email;
    this.languageService.getAll().subscribe(languages => {
      this.languages = languages;
      this.selectedLanguage = this.languageService.getById(
        this.authService.getUser().motherTongue.id
      );
    });
    this.sex = this.authService.currentUser.sex;
    this.birthday = this.authService.currentUser.birthday;
  }

  public saveGeneral(): void {
    if (this.usernameError.length == 0 && this.emailError.length == 0) {
      this.authService.currentUser.email = this.email;
      this.authService.currentUser.motherTongue = this.selectedLanguage;
      this.authService.currentUser.sex = this.sex;
      this.authService.currentUser.birthday = this.birthday;
      this.authService.saveUser();

      this.userService.update(this.authService.currentUser)
        .subscribe(() => {
          this.logger.debug('General setting saved successfully');
          localStorage.setItem('currentUser', JSON.stringify(this.authService.currentUser));
          // Materialize.toast('Changes saved successfully', 4000);
        });
    }
  }

  public checkUsername(): void {
    if (!User.usernameIsRight(this.username)) {
      this.usernameError = 'Username is required. Minimum 3 and ' +
        'maximum 30 characters.Letters, digits and @/./+/-/_ only.';
    } else {
      this.usernameError = '';
      if (this.username != this.authService.currentUser.username) {
        this.userService.getByParameter(this.username, 'username')
          .subscribe(user => {
            if (user != null) {
              this.usernameError = 'This user already exists';
            }
          });
      }
    }
  }

  public checkEmail(): void {
    if (!User.emailIsRight(this.email)) {
      this.emailError = 'You must type correct email';
    } else {
      this.emailError = '';
      if (this.email != this.authService.currentUser.email) {
        this.userService.getByParameter(this.email, 'email')
          .subscribe(user => {
            if (user != null) {
              this.emailError = 'This email is already found in our database';
            }
          });
      }
    }
  }
}
