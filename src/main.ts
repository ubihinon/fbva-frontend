import 'zone.js/dist/zone';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
// import './assets/favicons/favicons';
import {enableProdMode} from "@angular/core";
// import {processRules} from "@angular/compiler/src/shadow_css";

// if (process.env.ENV === 'production') {
//   enableProdMode();
// }

// console.log(ENV);

platformBrowserDynamic().bootstrapModule(AppModule);
