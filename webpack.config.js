const path = require('path');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtPlugin = require('script-ext-html-webpack-plugin');
const {AngularCompilerPlugin} = require('@ngtools/webpack');
const ProvidePlugin = require('webpack/lib/ProvidePlugin');
const HotModuleReplacementPlugin = require('webpack/lib/HotModuleReplacementPlugin');

const PATHS = {
    'source': path.join(__dirname, './src'),
    'build': path.join(__dirname, './build')
};

common = {
    entry: [
        'jquery',
        './src/main.ts',
        // './polyfills.ts',
    ],
    output: {
        path: __dirname + '/dist',
        filename: 'app.js',
    },
    resolve: {
        extensions: ['.ts', '.js', 'json']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: '@ngtools/webpack'
            },
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                }
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot)$/,
                loader: 'file-loader?name=assets/[name].[hash].[ext]'
            },
            {
                test: /\.component.scss$/,
                use: [
                    'to-string-loader',
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /(styles.scss|variables.scss|heading.css)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /.(png|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/,
                loader: 'url-loader?limit=100000'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + '/src/index.html',
            output: __dirname + '/dist',
            inject: 'head',
        }),
        new ScriptExtPlugin({
            defaultAttribute: 'defer'
        }),
        new AngularCompilerPlugin({
            tsConfigPath: './tsconfig.json',
            entryModule: './src/app/app.module#AppModule',
            sourceMap: true,
        }),
        new ProvidePlugin({
            "window.jQuery": "jquery",
            Hammer: "hammerjs/hammer"
        }),
    ]
};

devServer = function () {
    return {
        // module: {
        //     rules: [
        //         {
        //             test: /\.ts$/,
        //             enforce: 'pre',
        //             loader: 'tslint-loader',
        //             options: {/* Loader options go here */}
        //         }
        //     ]
        // },
        devtool: 'inline-source-map',
        optimization: {
            minimize: false
        },
        plugins: [
            new HotModuleReplacementPlugin(),
        ],
        'devServer': {
            port: 3000,
            hot: true,
            historyApiFallback: true,
            publicPath: '/',
            progress: true
        }
    }
};

module.exports = function (env) {
    console.log('Launched in', env, 'mode.');
    if (env == 'production') {
        return common;
    } else if (env == 'development') {
        return merge([common, devServer(),])
    }
};
